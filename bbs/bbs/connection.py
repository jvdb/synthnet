#!/usr/bin/env python3
# Jef Van den broeck 2022, 3-clause BSD
import random
import time
from bbs import application
from bbs import proto
from bbs.session import Session

# virtual
class Connection():
	def __init__(self, cb_output, cb_destroy):
		self.cb_output = cb_output
		self.cb_destroy = cb_destroy
		self.input_buffer = b''

	def input(self, data):
		self.input_buffer += data
		pass

	def teardown(self):
		pass

class DirectConnection(Connection):

	def mk_cb_output(self):
		def cb_output(data):
			print(f"info: direct connection outputting {len(data)} bytes")
			self.cb_output(data)
		return cb_output

	def mk_cb_destroy(self):
		def cb_destroy():
			print(f"info: direct connection destroying")
			self.cb_destroy()
		return cb_destroy

	def __init__(self, cb_output, cb_destroy):
		Connection.__init__(self, cb_output, cb_destroy)
		self.name = 'LOCAL'
		self.session = Session(self.mk_cb_output(), self.mk_cb_destroy(), self.name)

	def input(self, data):
		print(f"info: direct connection receiving {len(data)} bytes")
		#self.session.input(data.decode('utf-8').strip(), self.name)
		self.session.input(data, self.name)

	def teardown(self):
		print(f'debug: teardown, tearing down my session')
		self.session.teardown(self.name)

class PeerConnection(Connection):
	def __init__(self, cb_output, cb_destroy, my_name):
		Connection.__init__(self, cb_output, cb_destroy)
		self.my_name = my_name
		self.name = 'UNKNOWN'
		self.state = 'UNKNOWN'
		self.sessions = {}
		self.reassembly_buffer = b''

		self.timeout_ping = 5
		self.timeout_kill = 15
		self.last_heard_at = 0

	def up(self):
		self.last_heard_at = time.time()

		# send out an ident
		self.cb_output(proto.pack_hello(self.my_name))

	def mk_cb_output(self, session_handle):
		def cb_output(data):
			print(f"info: peer={self.name} session={session_handle} connection outputting {len(data)} bytes")
			self.cb_output(proto.pack_data(session_handle, data))
		return cb_output

	def mk_cb_destroy(self, session_handle):
		def cb_destroy():
			print(f"info: peer={self.name} session={session_handle} connection destroying")
			if session_handle in self.sessions:
				del self.sessions[session_handle]
				self.cb_output(proto.pack_close(session_handle))
		return cb_destroy

	def input(self, data):
		self.reassembly_buffer += data
		print(f"info: peer {self.name}/{self.state} receiving {len(data)} bytes, {len(self.reassembly_buffer)} buffered")
	
		try:
			while True:
				handle, opcode, msg, n_parsed = proto.unpack(self.reassembly_buffer)
				self.reassembly_buffer = self.reassembly_buffer[n_parsed:]
				self.handle_message(handle, opcode, msg)
		except proto.NotEnoughData:
			pass

	def handle_message(self, handle, opcode, msg):
		def session_handle(name, handle):
			return f'{name}_{handle}'

		self.last_heard_at = time.time()

		opcode_ascii = "".join([ k for k in proto.Opcode.keys() if proto.Opcode[k] == opcode ])

		print(f"info: peer {self.name}/{self.state} sent {opcode_ascii} msg={len(msg)}")

		if self.state == 'UNKNOWN':
			if opcode == proto.Opcode['HELLO']:
				print(f"info: peer identified as {msg}, promoting to UP")
				self.name = msg.decode('utf-8')
				self.state = 'UP'
			else:
				print(f"warn: peer didn't send HELLO but {opcode}, removing")
				# TODO more stack traversal
				self.cb_destroy()
		else:
			if opcode == proto.Opcode['PING']:
				self.cb_output(proto.pack_pong())
			elif opcode == proto.Opcode['PONG']:
				pass
			elif opcode == proto.Opcode['OPEN']:
				print(f"debug: peer sent OPEN handle={handle}")
				if handle in self.sessions:
					print(f"error: double session_handle={handle}")
					# todo kill other sessiona
				else:
					self.sessions[handle] = Session(self.mk_cb_output(handle),
						self.mk_cb_destroy(handle), session_handle(self.name, handle))
			elif opcode == proto.Opcode['CLOSE']:
				if handle not in self.sessions:
					print(f"error: no such session_handle={handle}")
				else:
					if handle in self.sessions:
						self.sessions[handle].teardown(self.name)
						del self.sessions[handle]
			elif opcode == proto.Opcode['DATA']:
				if handle not in self.sessions:
					print(f"error: no such session_handle={handle}")
					self.cb_output(proto.pack_close(handle))
				else:
					#self.sessions[handle].input(msg.decode('utf-8').strip(), self.name)
					self.sessions[handle].input(msg, session_handle(self.name, handle))
			else:
				print(f"error: unknown opcode={opcode}")

	def call(self, sessionish):
		handle = random.randint(0, 0xfffe)
		print(f"debug: created session {handle} for outbound call")
		self.sessions[handle] = sessionish
		self.cb_output(proto.pack_open(handle))
		return self.mk_cb_output(handle), self.mk_cb_destroy(handle)

	def housekeeping(self):
		now = time.time()
		if now > (self.last_heard_at + self.timeout_kill):
			print(f"debug: haven't heard in a long while, killing")
			self.cb_destroy()
			# TODO killall sessions
		if now > (self.last_heard_at + self.timeout_ping):
			print(f"debug: haven't heard in a while, sending ping")
			self.cb_output(proto.pack_ping())

	def teardown(self):
		print(f'debug: teardown, tearing down all sessions')
		for session_handle in self.sessions:
			print(f'debug: tearing down session {session_handle}')
			self.sessions[session_handle].teardown(self.my_name)

class ConnectionManager():

	# optimize later
	connections = []

	def __init__(self, my_name):
		self.my_name = my_name
		pass

	def bringup_peer(self, cb_output, cb_destroy):
		print(f"info: adding peer connection")
		connection = PeerConnection(cb_output, cb_destroy, self.my_name)
		self.connections.append(connection)
		return connection

	def teardown_peer(self, connection):
		print(f"info: removing peer connection")
		connection.teardown()
		self.connections.remove(connection)

	def bringup_direct(self, cb_output, cb_destroy):
		print(f"info: adding direct connection")
		connection = DirectConnection(cb_output, cb_destroy)
		self.connections.append(connection)
		return connection

	def teardown_direct(self, connection):
		print(f"info: removing direct connection")
		connection.teardown()
		self.connections.remove(connection)

	def get_peers(self):
		return [ c for c in self.connections
			if type(c) == PeerConnection ]
