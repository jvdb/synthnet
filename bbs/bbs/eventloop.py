#!/usr/bin/env python3
# Jef Van den broeck 2022, 3-clause BSD
import socket
import struct
import time
from select import poll, POLLIN, POLLOUT

"""
A shim, full Python'll happily f.e. recv(sock) whereas Micropython would
want recv(sock.fileno()).
"""
rich_sockets = True
try:
	import machine
	rich_sockets = False
except:
	pass
def compat_sock(s):
	return s if rich_sockets else s.fileno()

D = print

class FileDescriptorPool():
	"""
	A thin wrapper around a poll() fd set.
	"""

	p = None
	def __init__(self, pool):
		self.p = pool

	def add(self, fd, mode):
		self.p.register(fd, mode)

	def update(self, fd, mode):
		self.p.modify(fd, mode)

	def remove(self, fd):
		self.p.unregister(fd)

class SocketEventLoop():
	"""
	Accepts a list of Server()s, which it feeds a shared poll() fdset
	with .set_pool(), starts them up with .start(), and dispatches in the
	usual async fashion:
		- check whether a given active socket is theirs with .knows_fd()
		- if it is and it's marked readable, feed it with .readable()
		- if it is and it's marked writeable, feed it with .writeable().

	On every second-of-time rollover, call their .housekeeping()s.
	"""

	def __init__(self, servers):

		self.p = poll()
		self.pool = FileDescriptorPool(self.p)
		self.servers = servers

		for s in self.servers:
			s.set_pool(self.pool)
			s.start()

	def run(self):

		then = 0

		while True:
			for sock, flags in self.p.poll(500):
				sock = compat_sock(sock)
				for server in self.servers:
					if server.knows_fd(sock):
						if flags & POLLIN:
							server.readable(sock)
						elif flags & POLLOUT:
							server.writable(sock)
						else:
							print(f'weirdness, socket={sock.fileno()} has flags={flags}')
						break
				else:
					print(f'weirdness, can\'t find server for socket={sock}')

			# do a house-keeping check
			now = time.time()
			if int(now) > int(then):
				# a second passed, run it
				then = now
				for server in self.servers:
					server.housekeeping()
