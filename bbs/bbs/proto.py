#!/usr/bin/env python3
# Jef Van den broeck 2022, 3-clause BSD
import struct

D = print

Opcode = {
	'OPEN' : 1,
	'DATA' : 2,
	'CLOSE' : 3,
	'PING' : 4,
	'PONG' : 5,
	'HELLO' : 6
}

class NotEnoughData(Exception):
	pass

def unpack( data):
	header_len = struct.calcsize('<3H')
	if len(data) < header_len:
		raise NotEnoughData

	handle, opcode, msg_len = struct.unpack_from('<3H', data)

	if opcode not in [ Opcode['DATA'], Opcode['HELLO'] ]:
		return handle, opcode, '', header_len

	if len(data) < (header_len + msg_len):
		raise NotEnoughData

	return handle, opcode, data[header_len:header_len+ msg_len], header_len + msg_len

def pack_open(handle):
	return struct.pack('<3H', handle, Opcode['OPEN'], 0)
def pack_close(handle):
	return struct.pack('<3H', handle, Opcode['CLOSE'], 0)
def pack_data(handle, msg):
	return struct.pack('<3H', handle, Opcode['DATA'], len(msg)) + msg
def pack_ping():
	return struct.pack('<3H', 0, Opcode['PING'], 0)
def pack_pong():
	return struct.pack('<3H', 0, Opcode['PONG'], 0)
def pack_hello(ident):
	ident = bytes(ident, 'utf-8')
	return struct.pack('<3H', 0, Opcode['HELLO'], len(ident)) + ident
