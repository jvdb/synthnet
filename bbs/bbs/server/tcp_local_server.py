#!/usr/bin/env python3
# Jef Van den broeck 2022, 3-clause BSD
import socket
import struct
import time
from select import poll, POLLIN, POLLOUT

"""
A TCP local server.

Accepts raw TCP connections representing raw clients (e.g. netcatting humans),
binds any such incoming connections to a DirectConnection and relays traffic
appropriately.

When either the DirectConnection or the TCP client hangs up, we mark the client
as closed and let .housekeeping() forget it.
"""

D = print

class Server():
	fd_pool = None
	fd_listen = None
	clients = {}

	def __init__(self, connection_manager, hostname='', port=1111):
		self.connection_manager = connection_manager
		self.hostname = hostname
		self.port = port

	def set_pool(self, pool):
		self.fd_pool = pool

	def start(self):
		# fire up the socket, start listening
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.setblocking(False)
		s.bind((self.hostname, self.port))
		s.listen(10)

		self.fd_listen = s
		self.fd_pool.add(s, POLLIN)

	def knows_fd(self, fd):
		if fd == self.fd_listen.fileno():
			return True
		return fd in self.clients

	def readable(self, fd):
		if fd == self.fd_listen.fileno():
			self.accept()
		elif fd in self.clients:
			self.recv(fd)
		else:
			print(f"warn: unknown fd={fd}")

	def writable(self, fd):
		if fd == self.fd_listen.fileno():
			print(f'weirdness: writable server socket={fd}')
		elif fd in self.clients:
			client = self.clients[fd.fileno()]
			self.client_flush(client)
		else:
			print(f"warn: unknown fd={fd}")

	def client_forget(self, client):
		self.connection_manager.teardown_direct(client['connection'])
		self.fd_pool.remove(client['socket'])
		client['socket'].close()
		client['closed'] = True

	def client_flush(self, client):
		# try to send out what we have buffered, it's non-blocking so could
		# be nothing, but if it was everything mark the socket as not ready to send
		n_written = client['socket'].send(client['output_buffer'])
		client['output_buffer'] = client['output_buffer'][n_written:]
		if not len(client['output_buffer']):
			# sent entire buffer, we're not ready to send any more
			self.fd_pool.update(client['socket'], POLLIN)

	def accept(self):
		(s, a) = self.fd_listen.accept()
		s.setblocking(False)
		
		self.fd_pool.add(s, POLLIN)

		client = {
			'socket' : s,
			'address' : a,
			'output_buffer' : b'',
			'closed' : False,
			'connection' : None,
		}
		# closure for sending
		def mk_cb_output(client):
			def cb_output(data):
				# buffer and mark ready-to-send
				client['output_buffer'] += data
				self.fd_pool.update(client['socket'], POLLIN | POLLOUT)
				# try and flush what we can
				self.client_flush(client)
			return cb_output
		# closure for destroying
		def mk_cb_destroy(client):
			def cb_destroy():
				# try to flush out anything pending, then close it down
				self.client_flush(client)
				self.client_forget(client)
			return cb_destroy

		# save it and boot up the client, this may cause output
		client['connection'] = self.connection_manager.bringup_direct(
			mk_cb_output(client),
			mk_cb_destroy(client))

		self.clients[s.fileno()] = client
			
	def recv(self, fd):
		client = self.clients[fd]

		data = client['socket'].recv(1024)
		if not len(data):
			self.client_forget(client)
		else:
			client['connection'].input(data)

	def housekeeping(self):
		self.clients = {
			s : client
			for s, client in self.clients.items()
			if not client['closed']
		}

	def __str__(self):
		return f"TCPServer<n_clients={len(self.clients)}>"
