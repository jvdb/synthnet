#!/usr/bin/env python3
# Jef Van den broeck 2022, 3-clause BSD
import socket
import struct
import time
import random
from bbs import proto
from select import poll, POLLIN, POLLOUT

D = print

def only_if_connected(fn):
	def wrapped(*args, **kwargs):
		obj = args[0] # self
		if not obj.connected:
			print('no')
			return
		return fn(*args, **kwargs)
	return wrapped

class Server():
	fd_pool = None
	fd = None
	server = None

	def __init__(self, connection_manager, dest_hostname, dest_port):
		self.connection_manager = connection_manager
		self.dest_hostname = dest_hostname
		self.dest_port = dest_port
		self.output_buffer = b''
		self.connected = False
		self.last_connection_attempt_at = 0
		self.reconnection_timeout = 10
		self.fd = None

	def set_pool(self, pool):
		self.fd_pool = pool

	def start(self):
		self.connect()

	def knows_fd(self, fd):
		if self.fd != None:
			return self.fd.fileno() == fd

	def close(self):
		if self.fd != None:
			self.connected = False
			self.fd_pool.remove(self.fd)
			self.fd.close()
			self.fd = None
			if self.server:
				self.connection_manager.teardown_peer(self.server)
				self.server = None
			self.output_buffer = b''

	def connect(self):
		self.last_connection_attempt_at = time.time()
		self.close()

		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.setblocking(False)
		try:
			s.connect((self.dest_hostname, self.dest_port))
		except Exception as e:
			D(e)
			# connect on an async socket, this is normal, wait for it
			# to become writable and snatch the retcode with getsockopt
			pass
		self.fd = s
		self.fd_pool.add(s, POLLOUT)
		self.server = self.connection_manager.bringup_peer(
			self.cb_output, self.cb_destroy)

		self.connected = False

	def writable(self, fd):
		assert self.fd.fileno() == fd
		if not self.connected:
			# one would getsockopt(socket.SOL_SOCKET, socket.SO_ERROR) here and
			# assume the socket is connected if err = 0, but micropython has
			# no such thing, so we assume success and let housekeeping figure it out
			self.connected = True
			self.fd_pool.update(self.fd, POLLIN)
			self.server.up()
			return
		else:
			self.flush()

	def flush(self):
		try:
			n_written = self.fd.send(self.output_buffer)
			self.output_buffer = self.output_buffer[n_written:]
			if not len(self.output_buffer):
				# written it all, stop asking for writability
				self.fd_pool.update(self.fd, POLLIN)
		except Exception as e:
			print(f'error writing {e}')
			self.close()


	def readable(self, fd):
		assert self.fd.fileno() == fd
		if self.connected:
			self.recv()

	@only_if_connected
	def recv(self):
		if not self.connected:
			return

		try:
			data = self.fd.recv(1024)
	
			if len(data) == 0:
				raise Exception
		except:
			return self.connect()

		self.server.input(data)

	def send(self, data):
		self.output_buffer += data
		self.fd_pool.update(self.fd, POLLIN | POLLOUT)
		self.flush()

	@only_if_connected
	def cb_output(self, data):
		self.output_buffer += data
		self.fd_pool.update(self.fd, POLLIN | POLLOUT)
		self.flush()
	
	@only_if_connected
	def cb_destroy(self):
		self.flush()
		self.connect()

	def housekeeping(self):
		if not self.connected:
			now = time.time()
			should_reconnect = now > (self.last_connection_attempt_at + self.reconnection_timeout)
			if should_reconnect:
				self.connect()
		else:
			self.server.housekeeping()

	def __str__(self):
		return f"TCPUplinkServer<connected={self.connected}>"
