#!/usr/bin/env python3
# Jef Van den broeck 2022, 3-clause BSD
import socket
import struct
import time
import random
from bbs import proto
from select import poll, POLLIN, POLLOUT

D = print

class Server():
	fd_pool = None
	fd_listen = None

	def __init__(self, connection_manager, hostname, port):
		self.connection_manager = connection_manager
		self.hostname = hostname
		self.port = port 
		self.clients = {}

	def set_pool(self, pool):
		self.fd_pool = pool

	def start(self):
		# open up a port, start listening
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		s.setblocking(False)
		s.bind((self.hostname, self.port))
		s.listen(10)

		self.fd_listen = s
		self.fd_pool.add(s, POLLIN)

	def knows_fd(self, fd):
		if fd == self.fd_listen.fileno():
			return True
		return fd in self.clients

	def readable(self, fd):
		assert fd == self.fd_listen.fileno() or (fd in self.clients)
		if fd == self.fd_listen.fileno():
			self.accept()
		elif fd in self.clients:
			self.recv(fd)
		else:
			print(f"warn: unknown socket={fd}")

	def writable(self, fd):
		assert fd == self.fd_listen.fileno() or (fd in self.clients)
		if fd == self.fd_listen.fileno():
			print(f'warn: writable server socket={fd}')
		elif fd in self.clients:
			self.send(fd)
		else:
			print(f"warn: unknown socket={fd}")
	
	def accept(self):
		(s, a) = self.fd_listen.accept()
		s.setblocking(False)

		self.fd_pool.add(s, POLLIN)

		client = {
			'socket' : s,
			'address' : a,
			'output_buffer' : b'',
			'closed' : False,
			'connection' :None
		}

		def mk_cb_output(client):
			def cb_output(data):
				client['output_buffer'] += data
				self.fd_pool.update(client['socket'], POLLIN | POLLOUT)
				# try and flush what we can
				self.client_flush(client)
			return cb_output
		def mk_cb_destroy(client):
			def cb_destroy():
				# try to flush out anything pending, then close it down
				self.client_flush(client)
				self.client_forget(client)
			return cb_destroy

		client['connection'] = self.connection_manager.bringup_peer(
			mk_cb_output(client), mk_cb_destroy(client))
		client['connection'].up()

		self.clients[s.fileno()] = client

	def client_forget(self, client):
		self.connection_manager.teardown_peer(client['connection'])
		self.fd_pool.remove(client['socket'])
		client['socket'].close()
		client['closed'] = True

	def client_flush(self, client):
		try:
			n_written = client['socket'].send(client['output_buffer'])
			client['output_buffer'] = client['output_buffer'][n_written:]
			if not len(client['output_buffer']):
				# written it all, stop asking for writability
				self.fd_pool.update(client['socket'], POLLIN)
		except Exception as e:
			print(f'error: writing to client threw {e}')
			self.client_forget(client)

	def recv(self, fd):
		client = self.clients[fd]

		data = client['socket'].recv(1024)
		if not len(data):
			self.client_forget(client)
		else:
			client['connection'].input(data)

	def send(self, client, data):
		client['output_buffer'] += data
		self.fd_pool.update(client['socket'], POLLIN | POLLOUT)
		self.client_flush(client)

	def housekeeping(self):
		self.clients = {
			s : client
			for s, client in self.clients.items()
			if not client['closed']
		}

	def __str__(self):
		return f"TCPDownlinkServer<clients={self.clients}>"
