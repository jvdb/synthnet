#!/usr/bin/env python3
# Jef Van den broeck 2022, 3-clause BSD
from bbs import application

class SessionExistsError(Exception):
	pass
class SessionDoesNotExistError(Exception):
	pass

class Session():
	PROMPT = '> '
	apps = []
	apps.append(application.InfoManager())
	apps.append(application.HelpManager())
	apps.append(application.PartyLineManager())
	apps.append(application.HeardManager())
	apps.append(application.CallManager())

	application = None

	def __init__(self, cb_output, cb_exit, src):
		self.cb_output = cb_output
		self.cb_exit = cb_exit
		self.src = src
		self.say('Welcome to the BBS.')
		self.say(self.PROMPT, True)

	def application_exit(self):
		self.application = None
		self.say('\n' + self.PROMPT, True)

	def input(self, data, src):
		print(f'debug; application data={data} src={src}')
		if self.application:
			self.application.input(data, src)
			return

		data = data.decode('utf-8').strip()

		if data == '':
			pass
		if data == 'quit':
			self.say('byebye')
			self.cb_exit()
			return
		elif data == 'commands':
			self.say('i know:')
			for a in self.apps:
				self.say(a.KEYWORD)
		else:
			for app in self.apps:
				if data == app.KEYWORD:
					self.application = app.start(self.cb_output, self.application_exit, self.src)
					self.application.start()
					return
			else:
				self.say(f'unkown command {data}')
		self.say(self.PROMPT, True)

	def say(self, data, inhibit_newline=False):
		if not inhibit_newline:
			data = f'{data}\n'
		self.cb_output(bytes(data, 'utf-8'))
	
	def teardown(self, src):
		if self.application:
			self.application.teardown(src)
		pass
