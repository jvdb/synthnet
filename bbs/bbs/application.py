# Jef Van den broeck 2022, 3-clause BSD

connection_manager = None

class PartyLineManager():
	KEYWORD = "partyline"
	ARGS = []

	last_lines = ['\u2580\u2588\u2580\u2003\u2588\u2580\u2580\u2003\u2588\u2580\u2003\u2580\u2588\u2580\r\u2591\u2588\u2591\u2003\u2588\u2588\u2584\u2003\u2584\u2588\u2003\u2591\u2588\u2591', '\U0001f143\U0001f134\U0001f142\U0001f143', '\u30bb\U0001f757\u4e02\u30bb', '\u2500\u2554\u2557\u2500\u2500\u2500\u2500\u2500\u2500\u2554\u2557\r\u2554\u255d\u255a\u2557\u2500\u2500\u2500\u2500\u2554\u255d\u255a\u2557\r\u255a\u2557\u2554\u256c\u2550\u2550\u2566\u2550\u2569\u2557\u2554\u255d\r\u2500\u2551\u2551\u2551\u2551\u2550\u2563\u2550\u2550\x1b\x1b\x1b', '\u2500\u2554\u2557\u2500\u2500\u2500\u2500\u2500\u2500\u2554\u2557\r\u2554\u255d\u255a\u2557\u2500\u2500\u2500\u2500\u2554\u255d\u255a\u2557\r\u255a\u2557\u2554\u256c\u2550\u2550\u2566\u2550\u2569\u2557\u2554\u255d\r\u2500\u2551\u2551\u2551\u2551\u2550\u2563\u2550\u2550\u2563\u2551\r\u2500\u2551\u255a\u2563\u2551\u2550\u256c\u2550\u2550\u2551\u255a\u2557\r\u2500\u255a\u2550\u2569\u2550\u2550\u2569\u2550\u2550\u2569\u2550\u255d', '\u2500\u2554\u2557\u2500\u2500\u2500\u2500\u2500\u2500\u2554\u2557', '\u2554\u255d\u255a\u2557\u2500\u2500\u2500\u2500\u2554\u255d\u255a\u2557', '\u255a\u2557\u2554\u256c\u2550\u2550\u2566\u2550\u2569\u2557\u2554\u255d', '\u2500\u2551\u2551\u2551\u2551\u2550\u2563\u2550\u2550\u2563\u2551', '\u2500\u2551\u255a\u2563\u2551\u2550\u256c\u2550\u2550\u2551\u255a\u2557', '\u2500\u255a\u2550\u2569\u2550\u2550\u2569\u2550\u2550\u2569\u2550\u255d', 'HAR', '\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588', '\u2588\u2588\u2580\u2584\u2500\u2588\u2588\u2584\u2500\u2584\u2588\u2588\u2588\u2584\u2500\u2584\u2588\u2588\u2588\u2588\u2588\u2584\u2500\u2588\u2500\u2584\u2588\u2500\u2584\u2584\u2500\u2588\u2584\u2500\u2588\u2588\u2500\u2584\u2588\u2584\u2500\u2584\u2584\u2580\u2588\u2588\u2588\u2584\u2500\u2584\u2500\u2580\u2588\u2588\u2580\u2584\u2500\u2588\u2588\u2500\u2584\u2584\u2584\u2584\u2588\u2584\u2500\u2584\u2584\u2500\u2588', '\u2588\u2588\u2500\u2580\u2500\u2588\u2588\u2588\u2500\u2588\u2588\u2580\u2588\u2588\u2500\u2588\u2588\u2580\u2588\u2588\u2588\u2588\u2584\u2500\u2584\u2588\u2588\u2500\u2588\u2588\u2500\u2588\u2588\u2500\u2588\u2588\u2500\u2588\u2588\u2588\u2500\u2584\u2500\u2584\u2588\u2588\u2588\u2588\u2500\u2584\u2500\u2580\u2588\u2588\u2500\u2580\u2500\u2588\u2588\u2584\u2584\u2584\u2584\u2500\u2588\u2588\u2500\u2584\u2588\u2580\u2588', '\u2580\u2584\u2584\u2580\u2584\u2584\u2580\u2584\u2584\u2584\u2584\u2584\u2580\u2584\u2584\u2584\u2584\u2584\u2580\u2580\u2580\u2580\u2584\u2584\u2584\u2580\u2580\u2584\u2584\u2584\u2584\u2580\u2580\u2584\u2584\u2584\u2584\u2580\u2580\u2584\u2584\u2580\u2584\u2584\u2580\u2580\u2580\u2584\u2584\u2584\u2584\u2580\u2580\u2584\u2584\u2580\u2584\u2584\u2580\u2584\u2584\u2584\u2584\u2584\u2580\u2584\u2584\u2584\u2584\u2584\u2580', 'perfect']

	class Application():

		def __init__(self, manager, cb_output, cb_exit, src):
			self.manager = manager
			self.cb_output = cb_output
			self.cb_exit = cb_exit
			self.src = src

		def start(self):
			self.say('Welcome to the partyline:\n')
			self.dump_history()
			self.prompt()

		def dump_history(self):
			for line in self.manager.last_lines:
				self.say(' - ' + line + '\n')
			self.say('-end-\n')

		def input(self, msg, src):
			msg = msg.decode('utf-8').strip()
			if msg == '':
				self.dump_history()
			elif msg == 'help':
				self.say('an empty line shows the current messages\n')
				self.say('"help" shows you this help\n')
				self.say('"quit" stops the program\n')
			elif msg == 'quit':
				self.say('see you soon\n')
				self.cb_exit()
				return
			else:
				self.manager.last_lines.append(msg)
				self.dump_history()
			self.prompt()

		def say(self, data):
			self.cb_output(bytes(data, 'utf-8'))

		def prompt(self):
			self.say(f'{self.manager.KEYWORD}> ')

		def destroy(self):
			pass

	def __init__(self):
		pass

	def start(self, cb_output, cb_exit, src):
		return self.Application(self, cb_output, cb_exit, src)


class InfoManager():
	KEYWORD = "info"
	ARGS = []

	class Application():

		def __init__(self, manager, cb_output, cb_exit, src):
			self.manager = manager
			self.cb_output = cb_output
			self.cb_exit = cb_exit
			self.src = src

		def start(self):
			self.say('Welcome to the BBS. It\'s running on an\n')
			self.say('ESP32 somewhere in my living room, was\n')
			self.say('thusfar written in a weekend, and quietly\n')
			self.say('hopes to do for pre-internet networks what\n')
			self.say('synthwave did for the 80s.\n')
			self.say('Immediate plan:\n')
			self.say('* a message board, saving to EPROM\n')
			self.say('* "who is online"\n')
			self.say('* hacky non-WiFi networks\n')
			self.say('* peering to other BBSes, be the traceroute\n')
			self.say('* ultimately, stick this in a tree\n')
			self.say('Happy networking!\n')
			self.cb_exit()

		def say(self, data):
			self.cb_output(bytes(data, 'utf-8'))

		def input(self, msg, src):
			pass

		def destroy(self):
			pass

	def __init__(self):
		pass

	def start(self, cb_output, cb_exit, src):
		return self.Application(self, cb_output, cb_exit, src)


class HelpManager():
	KEYWORD = "help"
	ARGS = []

	class Application():

		def __init__(self, manager, cb_output, cb_exit, src):
			self.manager = manager
			self.cb_output = cb_output
			self.cb_exit = cb_exit
			self.src = src

		def start(self):

			self.say('help - prints this help\n')
			self.say('partyline - loads the partyline\n')
			self.say('info - prints information about this system\n')
			self.say('quit - logs you off\n')
			self.cb_exit()

		def say(self, data):
			self.cb_output(bytes(data, 'utf-8'))

		def input(self, msg, src):
			pass

		def destroy(self):
			pass

	def __init__(self):
		pass

	def start(self, cb_output, cb_exit, src):
		return self.Application(self, cb_output, cb_exit, src)

class HeardManager():
	KEYWORD = "heard"
	ARGS = []

	class Application():

		def __init__(self, manager, cb_output, cb_exit, src):
			self.manager = manager
			self.cb_output = cb_output
			self.cb_exit = cb_exit
			self.src = src

		def start(self):

			if connection_manager is None:
				self.say('not available\n')
			else:
				for c in connection_manager.get_peers():
					self.say(f'{c.name}\n')
			self.cb_exit()

		def say(self, data):
			self.cb_output(bytes(data, 'utf-8'))

		def input(self, msg, src):
			pass

		def destroy(self):
			pass

	def __init__(self):
		pass

	def start(self, cb_output, cb_exit, src):
		return self.Application(self, cb_output, cb_exit, src)

class CallManager():
	KEYWORD = "call"
	ARGS = [ "name" ]

	class Application():

		def __init__(self, manager, cb_output, cb_exit, src):
			self.manager = manager
			self.cb_output = cb_output
			self.cb_exit = cb_exit
			self.src = src

		def start(self):
			self.say('Starting call:\n')

			self.cb_call_output, self.cb_call_destroy = connection_manager.get_peers()[0].call(self)

		def cb_call_input(self, data):
			print(f'call input {len(data)} bytes')
			self.cb_output(data)

		def cb_call_done(self):
			print(f'call done')
			self.cb_exit()

		def input(self, msg, src):
			print(f'call in progress, msg={msg} src={src}')
			if src == self.src:
				print(f"msg from {src}, sending to call")
				# it's from the session we're running in
				self.cb_call_output(msg)
			else:
				print(f"msg from {src}, sending to local")
				# it's from the peer we're calling
				self.cb_output(msg)

		def say(self, data):
			self.cb_output(bytes(data, 'utf-8'))

		def teardown(self, src):
			if src == self.src:
				print(f'debug: call teardown from local, closing down call')
				self.cb_call_destroy()
				self.cb_exit()
			else:
				print(f'debug: call teardown from call, closing down local')
				self.say('remote peer closed connection')
				self.cb_exit()

	def __init__(self):
		pass

	def start(self, cb_output, cb_exit, src):
		return self.Application(self, cb_output, cb_exit, src)



class ApplicationManager():
	KEYWORD = "demo_application"
	ARGS = []

	class Application():

		def __init__(self, manager, cb_output, cb_exit, src):
			self.manager = manager
			self.cb_output = cb_output
			self.cb_exit = cb_exit

		def start(self):
			self.cb_output('hello\n')
			self.prompt()

		def input(self, msg, src):
			self.cb_output(f'thanks for {msg}\n')
			if msg == 'BYE':
				self.cb_output('goodbye!\n')
				self.cb_exit()
				return
			self.prompt()

		def prompt(self):
			self.cb_output(f'{self.manager.KEYWORD}> ')

		def teardown(self, src):
			pass

	def __init__(self):
		pass

	def start(self, cb_output, cb_exit, src):
		return self.Application(self, cb_output, cb_exit, src)
