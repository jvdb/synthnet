#!/usr/bin/env python3
# Jef Van den broeck 2022, 3-clause BSD
import network
import time
from bbs import eventloop
from bbs import connection
from bbs import application
from select import poll, POLLIN, POLLOUT

cfg = {
	'WIFI_SSID' : 'WOULDNTYOU',
	'WIFI_PASS' : 'LIKETOKNOW',
	'UPLINK_TCP_HOST' : 'SOMEHOST',
	'UPLINK_TCP_PORT' : '1',
	'TCP_PORT' : '1111'
}
with open('bbs.conf', 'r') as f:
	deets = { line[0] : line[1] for line in [
		line.strip().split('=') for line in f
	]}
	cfg.update(deets)

wlan = network.WLAN(network.STA_IF)
if not wlan.isconnected():
	wlan.active(True)
	wlan.connect(cfg['WIFI_SSID'], cfg['WIFI_PASS'])
	while not wlan.isconnected():
		print('.')
		time.sleep(1)
print(wlan.ifconfig())

cm = connection.ConnectionManager()
ev = eventloop.SocketEventLoop(servers=[
	eventloop.TCPServer(cm, port=int(cfg['TCP_PORT'])),
	eventloop.TCPUplinkServer(cm, cfg['UPLINK_TCP_HOST'], int(cfg['UPLINK_TCP_PORT']))
])

ev.run()
