#!/usr/bin/env python3
# Jef Van den broeck 2022, 3-clause BSD
from bbs import eventloop
from bbs import connection
from bbs import application
from bbs.server import tcp_local_server, tcp_uplink_server
from select import poll, POLLIN, POLLOUT
import time

cfg = {
	'UPLINK_TCP_HOST' : 'SOMEHOST',
	'UPLINK_TCP_PORT' : '1',
	'TCP_PORT' : '1111'
}
with open('bbs.conf', 'r') as f:
	deets = { line[0] : line[1] for line in [
		line.strip().split('=') for line in f
	]}
	cfg.update(deets)

cm = connection.ConnectionManager()
ev = eventloop.SocketEventLoop(servers=[
	tcp_local_server.Server(cm, port=int(cfg['TCP_PORT'])),
#	tcp_uplink_server.TCPUplinkServer(cm, cfg['UPLINK_TCP_HOST'], int(cfg['UPLINK_TCP_PORT']))
])

ev.run()
